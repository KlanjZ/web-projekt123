<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>OPG Klanjšček</title>
    <link rel="stylesheet" href="css/clanakstyle.css">
</head>

<body>
<?php include 'kreiraj_clanak.php'; ?>
    <nav class="navbar navbar-expand-lg bg-dark navbar-dark fixed-top">
        <div class="container">
            <a href="Pocetna.html" class="navbar-brand">OPG Klanjšček</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navmenu">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navmenu">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a href="Pocetna.html#learn" class="nav-link">O nama</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Članak</a>
                    </li>
                    <li class="nav-item">
                        <a href="lokacija.html" class="nav-link">Lokacija</a>
                    </li>
                    <li class="nav-item">
                        <a href="Iskustva_ljudi.html" class="nav-link">Iskustva drugih</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <?php displayArticles(); ?>
    </div>
</body>
</html>