<link rel="stylesheet" href="artikl.css">


<?php
function displayArticles() {
    $host = "localhost";
    $username = "root";
    $password = "";
    $database = "opgklanjscek_podatci";

    $conn = new mysqli($host, $username, $password, $database);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM articles ORDER BY id DESC";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo '<div class="article">';
            echo '<h2>' . $row['title'] . '</h2>';
            echo '<img src="' . $row['image_path'] . '" alt="Article Image">';
            echo '<p>' . $row['description'] . '</p>';
            echo '</div>';
        }
    } else {
        echo '<p>No articles available.</p>';
    }

    $conn->close();
}
?>