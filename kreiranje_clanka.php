<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kreiranje članka</title>
    <link rel="stylesheet" href="css/kreiranje.css">
</head>
<body>

<?php
session_start();


// Provjerite je li korisnik prijavljen kao administrator
if (isset($_SESSION['admin']) && $_SESSION['admin'] === true) {

    // Prikazi poruku o uspješnom kreiranju članka
    if (isset($_SESSION['article_created']) && $_SESSION['article_created']) {
        echo "<p style='color: green;'>Članak uspješno kreiran!</p>";
        $_SESSION['article_created'] = false; // Resetiraj varijablu nakon prikaza poruke
    }
?>


<!-- Ostatak obrasca za kreiranje članka -->
<form action="php/clanak_u_bazu.php" method="POST">
    <div class="container">
        <label for="title"><b>Naslov</b></label>
        <input type="text" placeholder="Upišite naslov" name="title" required>
        
        <label for="path"><b>Put do slike</b></label>
        <input type="text" placeholder="Upišite put do slike" name="path" required>
        
        <label for="describtion"><b>Opis</b></label>
        <textarea placeholder="Napišite opis" name="describtion" required></textarea>
        
        <button type="submit">Pošalji</button>
    </div>
</form>

<!-- Dodajte gumb za odjavu -->
<form action="odjava.php" method="POST">
    <button type="submit" class="logout custom-btn">Odjava</button>
</form>

<?php
} else {
    echo "Nemate pristup ovoj stranici. Prijavite se kao administrator.";
}
?>

</body>
</html>