-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2023 at 09:39 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opgklanjscek_podatci`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE `administrators` (
  `id` int(11) NOT NULL,
  `ime` varchar(255) NOT NULL,
  `sifra` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`id`, `ime`, `sifra`) VALUES
(1, 'klanjz', 'robert123');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `description`, `image_path`, `date`) VALUES
(4, 'Farma', 'ovdje na slici mozemo vidjeti razne krave na farmi kako mirno jedu i uzivaju u svojem vremenu sa drugim kravama', 'slike/Farma.jpg', '2023-12-02 22:58:27');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `ime` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `razlog` varchar(255) NOT NULL,
  `poruka` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `ime`, `email`, `razlog`, `poruka`) VALUES
(7, 'asddas', 'dsa@gmail.com', 'asdsad', 'dsad'),
(9, 'robert', 'robert_pavic_lover@gmail.com', 'zasto volim pavica', 'Volim Roberta Pavica zbog njegove nevjerojatne snalažljivosti, topline srca i sposobnosti da uvijek pronalazi radost i smijeh čak i u najizazovnijim trenucima.'),
(10, 'sddasd', 'ds@gmail.com', 'dasd', 'dwq'),
(11, 'ds', 'asdasd@gmail.com', 'dasdd', 'sad'),
(12, '', '', '', ''),
(13, '', '', '', ''),
(14, '', '', '', ''),
(15, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `iskustva`
--

CREATE TABLE `iskustva` (
  `id` int(11) NOT NULL,
  `ime` varchar(255) NOT NULL,
  `poruka` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

--
-- Dumping data for table `iskustva`
--

INSERT INTO `iskustva` (`id`, `ime`, `poruka`, `date`) VALUES
(48, 'sdasdsa', 'asdads', '2023-12-02 19:41:03'),
(65, 'dsad', 'asdads', '2023-12-02 19:44:30'),
(66, 'Jakov', 'Na C disku otvorite direktorij xampp i pokrenite datoteku xampp-control. Ovdje je\npotrebno pokrenuti Apache i MySQL module.\nb) Otvoriti preglednik i otići na adresu http://localhost/phpmyadmin i tamo stvoriti novu\nbazu podataka koja se zove \'skladiste\' s collation \'utf8mb4_croatian_ci\', kod u prilogu.\nc) Otvoriti direktorij htdocs i u njemu stvoriti novi direktorij pod nazivom lv7. U njemu\nstvoriti sve potrebne php skripte, kao na slici 1.\nd) Napraviti php skriptu za spoj na bazu skladište. Skriptu nazvati spoj.php. Za rad sa\nbazom obvezno je odabrati MySQLi Object-Oriented pristup. Potrebne informacije su:\n$servername = \"localhost\"; $username = \"root\"; $password = \"\"; $dbname = \"skladiste\";\ne) Napraviti indeks.php, početnu stranicu koja će se otvarati pri pozivu adrese\nhttp://localhost/lv7. Na tu naslovnu stranicu dodati samo naslov i dva linka (na\nprijava.php i registracija.php).\nf) Kada korisnik odabere da se prijavi otvori se skripta prijava.php. Ovdje je potrebno\nnapraviti formu za unos korisničkom imena i lozinke te ukoliko se isti nalaze u tablici\nkorisnici potrebno je preusmjeriti korisnika na stranicu dodaj_proizvod.php ako korisnik\nima ulogu \'admin\', ukoliko ima neku drugu ulogu potrebno ga je preusmjeriti na stranicu\nispis.php. Tu provjeru naprtaviti preko vari', '2023-12-02 19:48:26'),
(67, 'sadads', 'sdsa', '2023-12-02 19:48:45'),
(68, 'asdd a', 'aaaa', '2023-12-02 20:11:26'),
(69, 'ldsdas', 'ffff', '2023-12-02 20:32:26'),
(70, 'robert', 'jako sam lijep', '2023-12-02 20:50:05'),
(71, 'fsad', 'd', '2023-12-02 20:55:16'),
(72, 'zadnja', 'kako to', '2023-12-02 21:02:16'),
(73, 'robert pavic', 'obozavam roberta pavica ', '2023-12-02 21:31:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iskustva`
--
ALTER TABLE `iskustva`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrators`
--
ALTER TABLE `administrators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `iskustva`
--
ALTER TABLE `iskustva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
