

<?php
$host = "localhost";
$username = "root";
$password = "";
$database = "opgklanjscek_podatci";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $ime = $_POST["username"];
    $poruka = $_POST["message"];

    $conn = new mysqli($host, $username, $password, $database);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $stmt = $conn->prepare("INSERT INTO iskustva (ime, poruka) VALUES (?, ?)");
    $stmt->bind_param("ss", $ime, $poruka);

    if ($stmt->execute()) {
        echo "Poruka uspješno spremljena!";
    } else {
        echo "Greška prilikom spremanja poruke: " . $stmt->error;
    }

    $stmt->close();
    $conn->close();
}
?>