<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Page</title>
    <link rel="stylesheet" href="css/message.css">
</head>
<body>

<div class="container">
    <?php
    $host = "localhost";
    $username = "root";
    $password = "";
    $database = "opgklanjscek_podatci";

    $conn = new mysqli($host, $username, $password, $database);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT id, ime, poruka, DATE_FORMAT(date, '%d/%m/%Y %H:%i') AS formatted_date
            FROM iskustva
            ORDER BY id DESC";  // Order by ID in descending order

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo '<div class="message">';
            echo '<strong>' . $row['ime'] . '</strong><br>';
            echo '<p>' . $row['poruka'] . '</p>';
            echo '<p>Datum: ' . $row['formatted_date'] . '</p>'; // Display the formatted date with hours and minutes
            echo '</div>';
        }
    } else {
        echo '<div class="no-messages">Nema dostupnih poruka.</div>';
    }

    $conn->close();
    ?>
</div>

</body>
</html>