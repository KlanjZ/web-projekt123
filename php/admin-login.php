
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OPG Klanjšček</title>
    <link rel="stylesheet" href="../css/login.css">
</head>
<body>

<form action="" method="post">
    <div class="container">
        <label for="uname"><b>Korisnik</b></label>
        <input type="text" placeholder="Unesi korisničko ime" name="uname" required>

        <label for="psw"><b>Šifra</b></label>
        <input type="password" placeholder="Unesi šifru" name="psw" required>

        <button type="submit">Prijavi se</button>
    </div>
</form>


</body>
</html>


<?php
function processLogin() {
    session_start();
    $host = "localhost"; 
    $username = "root"; 
    $password = ""; 
    $database = "opgklanjscek_podatci"; 


    $conn = new mysqli($host, $username, $password, $database);


    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Handle login logic
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $uname = $_POST["uname"];
        $psw = $_POST["psw"];

        $stmt = $conn->prepare("SELECT * FROM administrators WHERE ime = ? AND sifra = ?");
        $stmt->bind_param("ss", $uname, $psw);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows == 1) {
            $_SESSION['admin'] = true;
            header("Location: http://localhost/web-projekt123/kreiranje_clanka.php");
        } else {
            echo "<p style='color: red;'>Pogrešno korisničko ime ili šifra.</p>";
        }

        $stmt->close();
    }

    $conn->close();
}

// Call the function when the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    processLogin();
}
?>
