<?php
session_start();

// Uništavanje sesije
session_destroy();

// Preusmjeravanje na stranicu prijave (možete postaviti drugu stranicu po potrebi)
header("Location: http://localhost/web-projekt123/Pocetna.html");
exit();
?>